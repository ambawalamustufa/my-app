import './App.css';
import {useState,useEffect} from 'react';
import Info from './Info';
function App() {

const [post,setpost]=useState([]);
useEffect(()=>
{
    const url='https://randomuser.me/api/?page=1&results=30';
    fetch(url)
    .then(data=>data.json())
    .then(data=>setpost(data.results))
},[]);

  return (
    <div className="App">
      {
      post.map(data=><Info 
        key={data.login.uuid}
        photo={data.picture.thumbnail} 
        fname={data.name.first} 
        lname={data.name.last} 
        email={data.email} 
        dob={data.dob.date}/>)
      }
    </div>
  );
}

export default App;

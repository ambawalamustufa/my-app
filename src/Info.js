import React from 'react'
import { Card } from 'react-bootstrap';
import './infocss.css';
const Info = (props) => {
    //formating date in ddmmyy format
    var str=props.dob;
    var s1=new Array(str.slice(0,10).split("-"));   
    var s2=s1[0][2]+"-"+s1[0][1]+"-"+s1[0][0];
    return (
        <div className="maindiv">
            <div className="innerdiv">

               <Card className="cardid"style={{ width: '18rem' }}>
                    <Card.Img variant="top" src={props.photo} />
                    <Card.Body className="cardbody">
                        <Card.Text>
                           <b>Name: </b>{props.fname+" "+props.lname}
                        </Card.Text>
                        <Card.Text>
                           <small><b>Email: </b></small> <span style={{fontSize:"13px"}}>{props.email}</span>
                        </Card.Text>
                        <Card.Text>
                        <b>Dob: </b>{s2}
                        </Card.Text>

                    </Card.Body>
                    </Card>
            </div>
        </div>
    )
}
export default Info